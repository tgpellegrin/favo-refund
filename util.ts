import moment from 'moment';

export const defaultDateFormat = 'YYYY_MM_DD';

export const formatDate = (date: string, format: string): string => {
  const momentDate = moment(date);
  if (momentDate.isValid()) {
    return momentDate.format(format);
  }

  const momentFormat = moment(date, format);
  if (momentFormat.isValid()) {
    return momentFormat.format(format);
  }

  return date;
};

export * from 'util';
