import 'reflect-metadata';
import Logger from '@dazn/lambda-powertools-logger';
import { APIGatewayProxyHandler } from 'aws-lambda';

import { CreateHttpFacade } from '../http/facade/CreateHttpFacade';
import { ParseAwsResult } from '../http/ParseAwsResult';

export const handler: APIGatewayProxyHandler = async (event) => {
  Logger.debug(`Received Event: ${JSON.stringify(event)}`);

  const facade = new CreateHttpFacade();
  const result = await facade.execute(event);

  Logger.debug(`Result: ${JSON.stringify(result)}`);

  return ParseAwsResult.parse(result);
};
