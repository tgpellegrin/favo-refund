import 'reflect-metadata';
import Logger from '@dazn/lambda-powertools-logger';

import { APIGatewayProxyHandler } from 'aws-lambda';
import { ReadHttpFacade } from '../http/facade/ReadHttpFacade';
import { ParseAwsResult } from '../http/ParseAwsResult';

export const handler: APIGatewayProxyHandler = async (event) => {
  Logger.debug(`Received Event: ${JSON.stringify(event)}`);

  const facade = new ReadHttpFacade();
  const result = await facade.execute(event);

  Logger.debug(`Result: ${JSON.stringify(result)}`);

  return ParseAwsResult.parse(result);
};
