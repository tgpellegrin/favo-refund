import { Container } from 'inversify';
import { Logger } from './application/contracts/Logger';
import { DaznLogger } from './domain/DaznLogger';
import { RefundRepository } from './application/contracts/repository/RefundRepository';
import { CreateCommandHandler } from './application/handlers/CreateCommandHandler';
import { UpdateCommandHandler } from './application/handlers/UpdateCommandHandler';
import { ReadCommandHandler } from './application/handlers/ReadCommandHandler';
import { RefundDynamoRepository } from './infrastructure/dynamodb/RefundDynamoRepository';

export class DependencyInjection {
  public static create(): Container {
    const container = new Container();

    container.bind(Logger).to(DaznLogger);

    // Repository
    container.bind(RefundRepository).to(RefundDynamoRepository);

    // Handlers
    container.bind(CreateCommandHandler).toSelf();
    container.bind(UpdateCommandHandler).toSelf();
    container.bind(ReadCommandHandler).toSelf();

    return container;
  }
}
