import { plainToClass, ClassConstructor } from 'class-transformer';

export const plainJsonToClass = <T>(type: ClassConstructor<T>, body: string): T => {
  const plainObject = JSON.parse(body);

  return plainToClass(type, plainObject);
};
