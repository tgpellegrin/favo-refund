export class HttpResponse {
  public body: unknown | null;

  public statusCode: number;

  public constructor(statusCode: number, body: unknown | null = null) {
    this.statusCode = statusCode;
    this.body = body;
  }
}
