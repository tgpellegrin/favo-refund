import { StatusCodes } from 'http-status-codes';
import { APIGatewayEvent } from 'aws-lambda';
import { Container } from 'inversify';
import { ValidationError as ContractError } from 'class-validator';
import { ApplicationEvent } from '../../domain/enums/ApplicationEvent';
import { HttpResponse } from '../response/HttpResponse';
import { ApplicationResult } from '../../domain/results/ApplicationResult';
import { Logger } from '../../application/contracts/Logger';
import { ValidationMessage } from '../../domain/results/ValidationMessage';

const parseStatus = {
  [ApplicationEvent.created]: StatusCodes.CREATED,
  [ApplicationEvent.invalid_execution]: StatusCodes.BAD_REQUEST,
  [ApplicationEvent.success]: StatusCodes.OK,
  [ApplicationEvent.not_found]: StatusCodes.NOT_FOUND,
};

const removeTarget = (items: ContractError[]): void => {
  items.forEach((item: ContractError) => {
    // eslint-disable-next-line no-param-reassign
    delete item.target;
    removeTarget(item.children);
  });
};

export abstract class BaseHttpFacade {
  public constructor(container: Container) {
    this.container = container;
    this.logger = this.container.get(Logger);
  }

  protected readonly container: Container;

  protected readonly logger: Logger;

  protected unauthorized = new HttpResponse(StatusCodes.UNAUTHORIZED);

  protected invalidRequest(errors: ContractError[]): HttpResponse {
    removeTarget(errors);

    const body: ValidationMessage = {
      code: 'APP-0001',
      message: 'Invalid request',
      details: errors,
    };

    return new HttpResponse(StatusCodes.BAD_REQUEST, body);
  }

  protected parseResult(applicationResult: ApplicationResult): HttpResponse {
    let status = parseStatus[applicationResult.event];

    if (status === StatusCodes.OK && applicationResult.data === null) {
      status = StatusCodes.NO_CONTENT;
    }

    return new HttpResponse(status, applicationResult.data);
  }

  public abstract execute(event: APIGatewayEvent): Promise<HttpResponse>;
}
