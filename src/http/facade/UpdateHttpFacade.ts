import { APIGatewayProxyEvent } from 'aws-lambda';
import { BaseHttpFacade } from './BaseHttpFacade';
import { HttpResponse } from '../response/HttpResponse';
import { UpdateRequest } from '../requests/UpdateRequest';
import { UpdateCommandHandler } from '../../application/handlers/UpdateCommandHandler';
import { DependencyInjection } from '../../DependencyInjection';

export class UpdateHttpFacade extends BaseHttpFacade {
  public constructor() {
    super(DependencyInjection.create());
  }

  public async execute(event: APIGatewayProxyEvent): Promise<HttpResponse> {
    try {
      const request = new UpdateRequest(event);

      this.logger.debug(`Request: ${JSON.stringify(request)}`);

      const validationErrors = await request.validate();

      if (validationErrors !== null) {
        this.logger.info('Invalid request');

        return this.invalidRequest(validationErrors);
      }

      const command = request.toCommand();

      this.logger.debug(`Command: ${JSON.stringify(command)}`);

      const handler = this.container.get(UpdateCommandHandler);

      const result = await handler.handle(command);

      return this.parseResult(result);
    } catch (error) {
      return new HttpResponse(200, JSON.stringify(error));
    }
  }
}
