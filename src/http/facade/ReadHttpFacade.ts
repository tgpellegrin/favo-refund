import { APIGatewayProxyEvent } from 'aws-lambda';

import { ReadCommandHandler } from '../../application/handlers/ReadCommandHandler';
import { HttpResponse } from '../response/HttpResponse';
import { ReadRequest } from '../requests/ReadRequest';
import { BaseHttpFacade } from './BaseHttpFacade';
import { DependencyInjection } from '../../DependencyInjection';

export class ReadHttpFacade extends BaseHttpFacade {
  public constructor() {
    super(DependencyInjection.create());
  }

  public async execute(event: APIGatewayProxyEvent): Promise<HttpResponse> {
    const request = new ReadRequest(event);

    this.logger.debug(`Request: ${JSON.stringify(request)}`);

    const validationErrors = await request.validate();

    if (validationErrors !== null) {
      this.logger.info('Invalid request');

      return this.invalidRequest(validationErrors);
    }

    const command = request.toCommand();

    this.logger.debug(`Command: ${JSON.stringify(command)}`);

    const handler = this.container.get(ReadCommandHandler);

    const result = await handler.handle(command);

    return this.parseResult(result);
  }
}
