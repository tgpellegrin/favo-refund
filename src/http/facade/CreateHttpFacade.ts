import { APIGatewayProxyEvent } from 'aws-lambda';
import { BaseHttpFacade } from './BaseHttpFacade';
import { HttpResponse } from '../response/HttpResponse';
import { CreateCommandHandler } from '../../application/handlers/CreateCommandHandler';
import { DependencyInjection } from '../../DependencyInjection';
import { CreateRequest } from '../requests/CreateRequest';

export class CreateHttpFacade extends BaseHttpFacade {
  public constructor() {
    super(DependencyInjection.create());
  }

  public async execute(event: APIGatewayProxyEvent): Promise<HttpResponse> {
    const request = new CreateRequest(event);

    this.logger.debug(`Request: ${JSON.stringify(request)}`);

    const validationErrors = await request.validate();

    if (validationErrors !== null) {
      this.logger.info('Invalid request');

      return this.invalidRequest(validationErrors);
    }

    const command = request.toCommand();

    this.logger.debug(`Command: ${JSON.stringify(command)}`);

    const handler = this.container.get(CreateCommandHandler);

    const result = await handler.handle(command);

    return this.parseResult(result);
  }
}
