import { APIGatewayProxyResult } from 'aws-lambda';
import { HttpResponse } from './response/HttpResponse';

export class ParseAwsResult {
  public static parse(response: HttpResponse): APIGatewayProxyResult {
    return {
      statusCode: response.statusCode,
      body: response.body === null || response.body === undefined ? '' : JSON.stringify(response.body),
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    };
  }
}
