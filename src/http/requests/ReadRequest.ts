import { IsDateString, IsOptional, IsString } from 'class-validator';
import { APIGatewayProxyEvent } from 'aws-lambda';

import { ReadCommand } from '../../application/commands/ReadCommand';
import { BaseRequest } from './BaseRequest';

export class ReadRequest extends BaseRequest {
  @IsString()
  @IsOptional()
  public leaderId?: string;

  @IsString()
  @IsOptional()
  public orderId?: string;

  @IsDateString()
  @IsOptional()
  public from?: string;

  @IsDateString()
  @IsOptional()
  public to?: string;

  public constructor(event: APIGatewayProxyEvent) {
    super(event);
    this.leaderId = event.pathParameters.leaderId;
    this.orderId = event.pathParameters.orderId;
    this.from = event.queryStringParameters.from;
    this.to = event.queryStringParameters.to;
  }

  public toCommand(): ReadCommand {
    return new ReadCommand(this.leaderId, this.orderId, this.from, this.to);
  }
}
