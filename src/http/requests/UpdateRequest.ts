import { Type } from 'class-transformer';
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { APIGatewayProxyEvent } from 'aws-lambda';

import { UpdateBody } from './body/UpdateBody';
import { UpdateCommand } from '../../application/commands/UpdateCommand';
import { plainJsonToClass } from '../../utils/operations';
import { BaseRequest } from './BaseRequest';

export class UpdateRequest extends BaseRequest {
  @IsString()
  @IsNotEmpty()
  public orderId!: string;

  @Type(() => UpdateBody)
  @ValidateNested()
  @IsNotEmpty()
  public body!: UpdateBody;

  public constructor(event: APIGatewayProxyEvent) {
    super(event);
    this.orderId = event.pathParameters.id;
    this.body = plainJsonToClass(UpdateBody, event.body);
  }

  public toCommand(): UpdateCommand {
    const items = this.body.items.map((item) => item.toEntity());
    const refund = this.body.refund.toEntity();

    return new UpdateCommand(this.orderId, items, refund);
  }
}
