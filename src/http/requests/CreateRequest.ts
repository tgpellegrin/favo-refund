import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { APIGatewayProxyEvent } from 'aws-lambda';

import { CreateBody } from './body/CreateBody';
import { CreateCommand } from '../../application/commands/CreateCommand';
import { plainJsonToClass } from '../../utils/operations';
import { BaseRequest } from './BaseRequest';
import { ItemInfoBody } from './body/ItemInfoBody';

export class CreateRequest extends BaseRequest {
  @Type(() => CreateBody)
  @ValidateNested()
  @IsNotEmpty()
  public body!: CreateBody;

  public constructor(event: APIGatewayProxyEvent) {
    super(event);
    this.body = plainJsonToClass(CreateBody, event.body);
  }

  public toCommand(): CreateCommand {
    const items = this.body.items.map((item: ItemInfoBody) => item.toEntity());
    const refund = this.body.refund.toEntity();
    const order = this.body.order.toEntity();
    const leader = this.body.leader.toEntity();

    return new CreateCommand(order.id, leader.id, items, refund);
  }
}
