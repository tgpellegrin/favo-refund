import { FavoHeaders } from '@favoapp/favo-eco';
import { Origin } from '@favoapp/favo-store-service';
import { APIGatewayEvent } from 'aws-lambda';
import { validateSync, ValidationError } from 'class-validator';

export abstract class BaseRequest {
  public origin: Origin;

  public accessToken: string;

  public sub: string;

  public constructor(event: APIGatewayEvent) {
    this.origin = this.getOrigin(event);

    this.accessToken = (event.headers?.Authorization ?? '').replace('Bearer ', '');

    this.sub = event.requestContext?.authorizer?.claims?.sub ?? '';
  }

  private getOrigin(event: APIGatewayEvent): Origin {
    if (event.headers === null || event.headers === undefined) {
      return Origin.BRAZIL;
    }

    const result =
      event.headers[FavoHeaders.X_ORIGIN_ID] ?? event.headers[FavoHeaders.X_ORIGIN_ID.toLowerCase()] ?? Origin.BRAZIL;

    return result as Origin;
  }

  public async validate(): Promise<ValidationError[] | null> {
    const errors = await validateSync(this);

    if (errors.length === 0) {
      return null;
    }

    return errors;
  }
}
