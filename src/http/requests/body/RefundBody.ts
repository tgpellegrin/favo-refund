import { Type } from 'class-transformer';
import { IsNotEmpty, IsString, ValidateIf, ValidateNested } from 'class-validator';

import { Refund } from '../../../domain/entities/Refund';
import { RefundType } from '../../../domain/enums/RefundType';
import { BeneficiaryBody } from './BeneficiaryBody';

export class RefundBody {
  @ValidateIf((value: RefundBody) => value.typeId in RefundType)
  @IsString()
  @IsNotEmpty()
  public typeId!: string;

  @IsString()
  @IsNotEmpty()
  public amount!: string;

  @Type(() => BeneficiaryBody)
  @ValidateNested()
  @IsNotEmpty()
  public beneficiary!: BeneficiaryBody;

  public toEntity(): Refund {
    return new Refund(this.typeId, this.amount, this.beneficiary);
  }
}
