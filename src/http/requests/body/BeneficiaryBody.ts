import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';

import { Beneficiary } from '../../../domain/entities/Beneficiary';

export class BeneficiaryBody {
  @IsString()
  @IsNotEmpty()
  public name!: string;

  @IsString()
  @IsNotEmpty()
  public documentNumber!: string;

  @IsNumberString()
  @IsNotEmpty()
  public bankCode!: string;

  @IsNumberString()
  @IsNotEmpty()
  public branchNumber!: string;

  @IsString()
  @IsNotEmpty()
  public accountNumber!: string;

  public toEntity(): Beneficiary {
    return new Beneficiary(this.name, this.documentNumber, this.bankCode, this.branchNumber, this.accountNumber);
  }
}
