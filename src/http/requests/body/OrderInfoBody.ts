import { IsBooleanString, IsNotEmpty, IsOptional, IsString } from 'class-validator';

import { OrderInfo } from '../../../domain/entities/OrderInfo';

export class OrderInfoBody {
  @IsString()
  @IsNotEmpty()
  public id!: string;

  @IsBooleanString()
  @IsOptional()
  public isOffer?: string;

  public toEntity(): OrderInfo {
    return new OrderInfo(this.id, this.isOffer);
  }
}
