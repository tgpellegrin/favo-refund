import { IsString, IsNotEmpty } from 'class-validator';

import { ItemInfo } from '../../../domain/entities/ItemInfo';

export class ItemInfoBody {
  @IsString()
  @IsNotEmpty()
  public sku: string;

  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsString()
  @IsNotEmpty()
  public qty: string;

  public toEntity(): ItemInfo {
    return new ItemInfo(this.sku, this.name, this.qty);
  }
}
