import { IsNotEmpty, IsString } from 'class-validator';

import { LeaderInfo } from '../../../domain/entities/LeaderInfo';

export class LeaderInfoBody {
  @IsString()
  @IsNotEmpty()
  public id!: string;

  public toEntity(): LeaderInfo {
    return new LeaderInfo(this.id);
  }
}
