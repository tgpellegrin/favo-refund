import { IsArray, IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

import { OrderInfoBody } from './OrderInfoBody';
import { LeaderInfoBody } from './LeaderInfoBody';
import { ItemInfoBody } from './ItemInfoBody';
import { RefundBody } from './RefundBody';

export class CreateBody {
  @Type(() => OrderInfoBody)
  @ValidateNested()
  @IsNotEmpty()
  order!: OrderInfoBody;

  @Type(() => LeaderInfoBody)
  @ValidateNested()
  @IsNotEmpty()
  leader!: LeaderInfoBody;

  @Type(() => ItemInfoBody)
  @ValidateNested()
  @IsArray()
  @IsNotEmpty()
  items!: ItemInfoBody[];

  @Type(() => RefundBody)
  @ValidateNested()
  @IsNotEmpty()
  refund!: RefundBody;
}
