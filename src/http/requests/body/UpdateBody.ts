import { Type } from 'class-transformer';
import { IsArray, IsNotEmpty, ValidateNested } from 'class-validator';

import { ItemInfoBody } from './ItemInfoBody';
import { RefundBody } from './RefundBody';

export class UpdateBody {
  @Type(() => ItemInfoBody)
  @ValidateNested()
  @IsArray()
  @IsNotEmpty()
  items!: ItemInfoBody[];

  @Type(() => RefundBody)
  @ValidateNested()
  @IsNotEmpty()
  refund!: RefundBody;
}
