import { ItemInfo } from '../../domain/entities/ItemInfo';
import { Refund } from '../../domain/entities/Refund';

export class UpdateCommand {
  public orderId: string;

  public items: ItemInfo[];

  public refund: Refund;

  public constructor(orderId: string, items: ItemInfo[], refund: Refund) {
    this.orderId = orderId;
    this.items = items;
    this.refund = refund;
  }
}
