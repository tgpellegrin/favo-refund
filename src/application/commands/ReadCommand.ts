export class ReadCommand {
  public leaderId: string;

  public orderId?: string;

  public from?: string;

  public to?: string;

  public constructor(leaderId: string, orderId: string, from: string, to: string) {
    this.leaderId = leaderId;
    this.orderId = orderId;
    this.from = from;
    this.to = to;
  }
}
