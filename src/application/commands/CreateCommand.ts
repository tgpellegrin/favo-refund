import { ItemInfo } from '../../domain/entities/ItemInfo';
import { Refund } from '../../domain/entities/Refund';

export class CreateCommand {
  public orderId: string;

  public leaderId: string;

  public items: ItemInfo[];

  public refund: Refund;

  public constructor(orderId: string, leaderId: string, items: ItemInfo[], refund: Refund) {
    this.orderId = orderId;
    this.leaderId = leaderId;
    this.items = items;
    this.refund = refund;
  }
}
