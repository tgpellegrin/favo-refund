import { inject, injectable } from 'inversify';

import { Logger } from '../contracts/Logger';
import { ApplicationResult } from '../../domain/results/ApplicationResult';
import { ApplicationEvent } from '../../domain/enums/ApplicationEvent';
import { UpdateCommand } from '../commands/UpdateCommand';
import { RefundRepository } from '../contracts/repository/RefundRepository';

@injectable()
export class UpdateCommandHandler {
  @inject(Logger)
  private readonly logger!: Logger;

  @inject(RefundRepository)
  private readonly repository!: RefundRepository;

  public async handle(command: UpdateCommand): Promise<ApplicationResult> {
    this.logger.info(`Verify if refund exists by order id: ${command.orderId}`);

    const refund = await this.repository.getByOrder(command.orderId);

    if (refund === null || refund === undefined) {
      this.logger.info('Refund not found');

      return ApplicationResult.notFound();
    }

    this.logger.info('Update issued refund');
    await this.repository.upsert(command.orderId, null, command.items, command.refund, false);

    return new ApplicationResult(ApplicationEvent.success);
  }
}
