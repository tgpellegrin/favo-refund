import { inject, injectable } from 'inversify';

import { Logger } from '../contracts/Logger';
import { ApplicationResult } from '../../domain/results/ApplicationResult';
import { ApplicationEvent } from '../../domain/enums/ApplicationEvent';
import { CreateCommand } from '../commands/CreateCommand';
import { RefundRepository } from '../contracts/repository/RefundRepository';
import { IssuedRefund } from '../../domain/results/create/IssuedRefund';

@injectable()
export class CreateCommandHandler {
  @inject(Logger)
  private readonly logger!: Logger;

  @inject(RefundRepository)
  private readonly repository!: RefundRepository;

  public async handle(command: CreateCommand): Promise<ApplicationResult> {
    this.logger.info(`Verify if refund exists by order id: ${command.orderId}`);

    const refund = await this.repository.getByLeader(command.leaderId);

    if (refund !== null || refund !== undefined) {
      this.logger.info('Refund already issued');

      return ApplicationResult.invalid('Refund already issued');
    }

    this.logger.info('Issue refund');
    await this.repository.upsert(command.leaderId, command.orderId, command.items, command.refund);

    return new ApplicationResult(ApplicationEvent.success, new IssuedRefund(command.orderId));
  }
}
