import { inject, injectable } from 'inversify';

import { Logger } from '../contracts/Logger';
import { ApplicationResult } from '../../domain/results/ApplicationResult';
import { ApplicationEvent } from '../../domain/enums/ApplicationEvent';
import { ReadCommand } from '../commands/ReadCommand';
import { RefundRepository } from '../contracts/repository/RefundRepository';

@injectable()
export class ReadCommandHandler {
  @inject(Logger)
  private readonly logger!: Logger;

  @inject(RefundRepository)
  private readonly refundRepository!: RefundRepository;

  public async handle(command: ReadCommand): Promise<ApplicationResult> {
    if (command.orderId) {
      this.logger.info(`Find refund by order id: ${command.orderId}`);

      const refund = await this.refundRepository.getByOrder(command.orderId, command.from, command.to);

      if (refund === null || refund === undefined) {
        return ApplicationResult.notFound();
      }

      return new ApplicationResult(ApplicationEvent.success, refund);
    }

    if (command.leaderId) {
      this.logger.info(`Find refund by leader id: ${command.leaderId}`);

      const refund = await this.refundRepository.getByLeader(command.leaderId, command.from, command.to);

      if (refund === null || refund === undefined) {
        return ApplicationResult.notFound();
      }

      return new ApplicationResult(ApplicationEvent.success, refund);
    }

    return new ApplicationResult(ApplicationEvent.invalid_execution);
  }
}
