import { ItemInfo } from '../../../domain/entities/ItemInfo';
import { Refund } from '../../../domain/entities/Refund';

export abstract class RefundRepository {
  public abstract upsert(
    leaderId: string,
    orderId: string,
    items?: ItemInfo[],
    refund?: Refund,
    newEntry?: boolean,
    status?: string,
  ): Promise<void>;

  public abstract getByOrder(id: string, from?: string, to?: string): Promise<Refund[]>;

  public abstract getByLeader(id: string, from?: string, to?: string): Promise<Refund[]>;
}
