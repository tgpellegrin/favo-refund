export abstract class Logger {
  public abstract info(message: string, params?: Record<string, unknown>): void;

  public abstract debug(message: string, params?: Record<string, unknown>): void;
}
