import { injectable } from 'inversify';
import moment from 'moment';
import { DynamoDB } from 'aws-sdk';
import { DocumentClient } from 'aws-sdk/lib/dynamodb/document_client';

import { formatDate, defaultDateFormat } from '../../../util';
import { Refund } from '../../domain/entities/Refund';
import { RefundRepository } from '../../application/contracts/repository/RefundRepository';
import { ItemInfo } from '../../domain/entities/ItemInfo';

const REFUND_SK = 'REFUND';

@injectable()
export class RefundDynamoRepository implements RefundRepository {
  public async upsert(
    orderId: string,
    leaderId?: string,
    items?: ItemInfo[],
    refund?: Refund,
    newEntry = true,
    status = 'OPEN',
  ): Promise<void> {
    const { FAVO_TABLE_CRM } = process.env;
    const documentClient = new DynamoDB.DocumentClient();

    const params: DynamoDB.DocumentClient.PutItemInput = {
      TableName: <string>FAVO_TABLE_CRM,
      Item: {
        PK: orderId,
        Data: `${status}${formatDate(moment().toISOString(), defaultDateFormat)}`,
        ...(newEntry && { SK: leaderId ? `${REFUND_SK}#${leaderId}` : REFUND_SK }),
        ...(newEntry && { CreateDateTime: moment().toISOString() }),
        ...(items && { Items: items }),
        ...(refund.beneficiary && { Beneficiary: refund.beneficiary }),
        ...(refund.typeId && { TypeId: refund.typeId }),
        ...(refund.amount && { Amount: refund.amount }),
      },
      ...(!newEntry && { ConditionExpression: `begins_with (SK, ${REFUND_SK})` }),
    };

    await documentClient.put(params).promise();
  }

  public async getByOrder(id: string, from?: string, to?: string): Promise<Refund[]> {
    const { FAVO_TABLE_CRM } = process.env;

    const params: DocumentClient.QueryInput = {
      TableName: <string>FAVO_TABLE_CRM,
      KeyConditionExpression: 'PK = :pkValue AND begins_with(SK, :skValue)',
      ExpressionAttributeValues: {
        ':pkValue': id,
        ':skValue': REFUND_SK,
      },
    };

    const documentClient = new DynamoDB.DocumentClient();
    const { Items } = await documentClient.query(params).promise();

    if (Items === null || Items === undefined || Items.length === 0) {
      return [];
    }

    return Items.filter((item) => {
      if (from && to)
        return (
          new Date(item.CreateDateTime).getTime() >= new Date(from).getTime() &&
          new Date(item.CreateDateTime).getTime() <= new Date(to).getTime()
        );
      if (from) return new Date(item.CreateDateTime).getTime() <= new Date(from).getTime();
      if (to) return new Date(item.CreateDateTime).getTime() <= new Date(to).getTime();

      return true;
    }).map((item) => item as Refund);
  }

  public async getByLeader(id: string, from?: string, to?: string): Promise<Refund[]> {
    const { FAVO_TABLE_CRM } = process.env;

    const params: DocumentClient.QueryInput = {
      TableName: <string>FAVO_TABLE_CRM,
      IndexName: 'GSI-1-Data',
      KeyConditionExpression: 'SK = :skValue',
      ExpressionAttributeValues: {
        ':skValue': `${REFUND_SK}#${id}`,
      },
    };

    const documentClient = new DynamoDB.DocumentClient();
    const { Items } = await documentClient.query(params).promise();

    if (Items === null || Items === undefined || Items.length === 0) {
      return [];
    }

    return Items.filter((item) => {
      if (from && to)
        return (
          new Date(item.CreateDateTime).getTime() >= new Date(from).getTime() &&
          new Date(item.CreateDateTime).getTime() <= new Date(to).getTime()
        );
      if (from) return new Date(item.CreateDateTime).getTime() <= new Date(from).getTime();
      if (to) return new Date(item.CreateDateTime).getTime() <= new Date(to).getTime();

      return true;
    }).map((item) => item as Refund);
  }
}
