export enum RefundType {
  FAVO_TO_CLIENT = 1,
  FAVO_TO_LEADER = 2,
  ORDER_TO_NEXT_DAY = 3,
}
