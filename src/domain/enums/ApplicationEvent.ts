/* eslint-disable camelcase */
export enum ApplicationEvent {
  success = 'success',
  invalid_execution = 'invalid_execution',
  created = 'created',
  not_found = 'not_found',
}
