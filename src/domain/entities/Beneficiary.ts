export class Beneficiary {
  public name: string;

  public documentNumber: string;

  public bankCode: string;

  public branchNumber: string;

  public accountNumber: string;

  public constructor(
    name: string,
    documentNumber: string,
    bankCode: string,
    branchNumber: string,
    accountNumber: string,
  ) {
    this.name = name;
    this.documentNumber = documentNumber;
    this.bankCode = bankCode;
    this.branchNumber = branchNumber;
    this.accountNumber = accountNumber;
  }
}
