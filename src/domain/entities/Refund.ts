import { Beneficiary } from './Beneficiary';

export class Refund {
  public typeId: string;

  public amount: string;

  public beneficiary: Beneficiary;

  public constructor(typeId: string, amount: string, beneficiary: Beneficiary) {
    this.typeId = typeId;
    this.amount = amount;
    this.beneficiary = beneficiary;
  }
}
