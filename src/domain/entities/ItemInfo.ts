export class ItemInfo {
  public sku: string;

  public name: string;

  public qty: string;

  public constructor(sku: string, name: string, qty: string) {
    this.sku = sku;
    this.name = name;
    this.qty = qty;
  }
}
