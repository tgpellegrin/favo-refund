export class OrderInfo {
  public id: string;

  public isOffer: string;

  public constructor(id: string, isOffer = 'false') {
    this.id = id;
    this.isOffer = isOffer;
  }
}
