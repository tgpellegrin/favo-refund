import { ValidationMessage } from './ValidationMessage';

export const InvalidDocument: ValidationMessage = {
  code: '0001',
  message: 'Invalid document',
};
