export class SearchResult<T> {
  protected resultValue: T | null;

  public constructor(value: T | null) {
    this.resultValue = value;
  }

  public static Empty<T>(): SearchResult<T> {
    return new SearchResult<T>(null);
  }

  public static Build<T>(value: T): SearchResult<T> {
    return new SearchResult<T>(value);
  }

  public isEmpty(): boolean {
    return this.resultValue === null;
  }

  public hasValue(): boolean {
    return this.resultValue !== null;
  }

  public get value(): T {
    return this.resultValue as T;
  }
}
