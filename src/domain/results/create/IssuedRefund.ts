export class IssuedRefund {
  public id: string;

  public constructor(id: string) {
    this.id = id;
  }
}
