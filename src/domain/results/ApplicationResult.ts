import { ApplicationEvent } from '../enums/ApplicationEvent';

export class ApplicationResult<T = unknown> {
  public event: ApplicationEvent;

  public data: T | null;

  public constructor(event: ApplicationEvent, data: T | null = null) {
    this.event = event;
    this.data = data;
  }

  public static invalid<T>(data: T | null): ApplicationResult {
    return new ApplicationResult(ApplicationEvent.invalid_execution, data);
  }

  public static success<T>(data: T | null = null): ApplicationResult {
    return new ApplicationResult(ApplicationEvent.success, data);
  }

  public static notFound(): ApplicationResult {
    return new ApplicationResult(ApplicationEvent.not_found);
  }
}
