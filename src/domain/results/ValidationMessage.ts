export interface ValidationMessage {
  code: string;
  message: string;
  errorType?: string;
  details?: unknown;
}
