import { injectable } from 'inversify';
import logger from '@dazn/lambda-powertools-logger';
import { Logger } from '../application/contracts/Logger';

@injectable()
export class DaznLogger implements Logger {
  public info(message: string, params?: Record<string, unknown>): void {
    logger?.info(message, params);
  }

  public debug(message: string, params?: Record<string, unknown>): void {
    logger?.debug(message, params);
  }
}
